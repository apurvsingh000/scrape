from django.shortcuts import render , redirect
from django.http import HttpResponse
from .forms import UrlForm
from .models import Movie , Role
import requests
import csv
import re
from lxml import html
# Create your views here.


def home_view(request):
    form = UrlForm(request.POST or None)
    if form.is_valid():
        url = form.cleaned_data.get('url_link')
        site = requests.get(url)
        page = html.fromstring(site.content)
        name= page.xpath('//div[@class="title_wrapper"]/h1/text()')
        name=name[0]
        star =page.xpath('//*[@id="title-overview-widget"]/div[1]/div[2]/div/div[1]/div[1]/div[1]/strong/span/text()')
        star =star[0]
        cast = page.xpath('//*[@class="odd"]/td[2]/a/text() | //*[@class="even"]/td[2]/a/text()')
        character= page.xpath('//*[@class="character"]/a/text() | //*[@class="character"]/a/text() | //tr/*[@class="character"]/text()')
        cast = [re.sub('\n', ' ' ,cast) for cast in cast]
        character = [re.sub(' \n                  \n          ', '',character) for character in character]
        while '' in character:
            character.remove('')
        while '\n            ' in character:
            character.remove('\n            ')
        character = [re.sub('\n                  \n          ', 'Unknown',character) for character in character]
        character = [re.sub('\n            ', '',character) for character in character]
        content ={}
        for i,cast in enumerate(cast,start=0):
            content[cast]=character[i]
        db= Movie(movie_name=name ,movie_star=star)
        db.save()
        for cast,character in content.items():
            db1= Role(cast=db,name=cast,character=character)
            db1.save()
        return redirect('product')
    else:
        form= UrlForm()
    return render(request, 'home.html', {'form': form})




def product_view(request):
    Movie.objects.get
    return render(request,"product.html", {})
