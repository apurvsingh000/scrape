from django.db import models

# Create your models here.
class Movie(models.Model):
    movie_name = models.CharField(max_length=100,primary_key=True)
    movie_star = models.FloatField(max_length=15)
    def __str__(self):
        return self.movie_name

class Role(models.Model):
    cast = models.ForeignKey(Movie,on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    character = models.CharField(max_length=100)
    def __str__(self):
        return self.cast.movie_name
